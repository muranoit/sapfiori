sap.ui.define([
	"sap/ui/core/mvc/Controller"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller) {
		"use strict";

		return Controller.extend("logaligroup.invoices.controller.mainview", {
			onInit: function () {
                const oJSONModel = new sap.ui.model.json.JSONModel();
                const oView = this.getView();

                oJSONModel.loadData("./model/SelectionScreenMenu.json")
                oView.setModel(oJSONModel, "selectionScreen");
			}
		});
	});
